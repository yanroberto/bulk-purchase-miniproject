FROM openjdk:17-oracle
# VOLUME /tmp
# ARG JAVA_OPTS
# ENV JAVA_OPTS=$JAVA_OPTS
WORKDIR /opt/app
COPY target/bulk-purchase-miniproject-0.0.1-SNAPSHOT.jar bulkpurchaseminiproject.jar
EXPOSE 8080
ENTRYPOINT exec java -jar bulkpurchaseminiproject.jar
