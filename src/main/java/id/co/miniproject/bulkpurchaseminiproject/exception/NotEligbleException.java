package id.co.miniproject.bulkpurchaseminiproject.exception;

import id.co.miniproject.bulkpurchaseminiproject.config.variabel.AppConstant;
import org.springframework.http.HttpStatus;

public class NotEligbleException   extends CommonException{


    public NotEligbleException(String phoneNumber) {
        super(AppConstant.COMPLETION_STATUS.BUSINESS_ERROR,
                "04",
                "SubsriberNotActive",
                "Customer " + phoneNumber + " is not eligble",
                "Phone Number "  + phoneNumber +  "is invalid for purchase due to not eligble subscriber",
                HttpStatus.UNPROCESSABLE_ENTITY);
    }

}
