package id.co.miniproject.bulkpurchaseminiproject.exception;

import id.co.miniproject.bulkpurchaseminiproject.config.variabel.AppConstant;
import org.springframework.http.HttpStatus;

public class DataMemberNotFound extends CommonException{

    public DataMemberNotFound() {
        super(AppConstant.COMPLETION_STATUS.BUSINESS_ERROR,
                "01",
                "00",
                "Data is not found",
                "Record Not Found",
                HttpStatus.NOT_FOUND);
    }
}
