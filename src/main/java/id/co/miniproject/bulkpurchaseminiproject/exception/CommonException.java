package id.co.miniproject.bulkpurchaseminiproject.exception;

import id.co.miniproject.bulkpurchaseminiproject.config.variabel.AppConstant;
import id.co.miniproject.bulkpurchaseminiproject.model.response.ApiFault;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

@Getter
@Setter
public class CommonException extends Exception{

    private AppConstant.COMPLETION_STATUS  status;
    private String code;
    private String type;
    private String exCode;
    private String displayMessage;
    private HttpStatus httpStatus;

    public CommonException(AppConstant.COMPLETION_STATUS status, String code, String type, String displayMessage, String message, HttpStatus httpStatus) {
        super(message);
        this.status = status;
        this.code = code;
        this.type = type;
        this.displayMessage = displayMessage;
        this.httpStatus = httpStatus;
    }

    public CommonException(Exception e) {
        super(e.getMessage());
        this.status = AppConstant.COMPLETION_STATUS.SYSTEM_ERROR;
        this.code = "99";
        this.type = e.getClass().getSimpleName();
        this.displayMessage = "Error caused: " + e.getClass().getSimpleName();
        this.httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
    }


    public CommonException(HttpStatus httpStatus, String code, String type, String displayMessage, String message) {
        super(message);
        this.status = AppConstant.COMPLETION_STATUS.valueOf(httpStatus.is5xxServerError() ? "SYSTEM_ERROR" : "BUSINESS_ERROR");
        this.httpStatus = httpStatus;
        this.code = code;
        this.type = type;
        this.displayMessage = displayMessage;
    }

    public CommonException(String message, Throwable cause, String exCode, HttpStatus httpStatus) {
        super(message, cause);
        this.exCode = exCode;
        this.httpStatus = httpStatus;
    }

    public ApiFault getApiFault() {
        return ApiFault.builder()
                .status(status)
                .code(code)
                .type(type)
                .detail(type + ":" + super.getMessage())
                .build();
    }

}
