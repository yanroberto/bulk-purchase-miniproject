package id.co.miniproject.bulkpurchaseminiproject.controller;


import id.co.miniproject.bulkpurchaseminiproject.model.entity.Product;
import id.co.miniproject.bulkpurchaseminiproject.model.request.ProductRq;
import id.co.miniproject.bulkpurchaseminiproject.model.request.SearchRq;
import id.co.miniproject.bulkpurchaseminiproject.model.response.ResponseData;
import id.co.miniproject.bulkpurchaseminiproject.service.ProductService;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/product")
public class ProductController {



    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private ProductService productService;

    @Operation(summary = "service to create new product")
    @PostMapping("/create-product")
    public ResponseEntity<ResponseData<Product>> create(@Valid @RequestBody ProductRq productRq, Errors errors){
        ResponseData<Product> responseData = new ResponseData<>();

        if(errors.hasErrors()){
            for(ObjectError error : errors.getAllErrors()) {
                responseData.getMessage().add(error.getDefaultMessage());
            }
            responseData.setStatus(false);
            responseData.setPayload(null);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseData);
        }

        Product productInfo = modelMapper.map(productRq,Product.class);

        responseData.setStatus(true);
        responseData.setPayload(productService.createNewProduct(productInfo));
        return ResponseEntity.ok(responseData);

    }

    @Operation(summary = "Get All Product Info")
    @GetMapping("/api/productInfo")
    public Iterable<Product>findAll(){
        return productService.findAll();
    }

    @Operation(summary = "Get Product Info By ID")
    @GetMapping("/{id}")
    public Product findOne(@PathVariable("id") Long id){
        return productService.findOne(id);
    }


    @Operation(summary = "Delete Product Info By ID")
    @DeleteMapping("/{id}")
    public void removeOne(@PathVariable("id") Long id){
        productService.removeOne(id);
    }

    @Operation(summary = "find Product Info By productCode")
    @PostMapping("/search/productCode")
    public Product getProductByName(@RequestBody SearchRq searchRq){
        return productService.findProductByProductCode(searchRq.getSearchKey());

    }

    @Operation(summary = "find Product Info By name")
    @PostMapping("/search/name-like")
    public List<Product> getProductByNameLike(@RequestBody SearchRq searchDTO){
        return productService.findProductByNameLike(searchDTO.getSearchKey());
    }


    @Operation(summary = "Update product info")
    @PutMapping
    public ResponseEntity<ResponseData<Product>> updateProduct(@Valid @RequestBody  Product product, Errors errors){
        ResponseData<Product> responseData = new ResponseData<>();

        if(errors.hasErrors()){
            for(ObjectError error : errors.getAllErrors()) {
                responseData.getMessage().add(error.getDefaultMessage());
            }
            responseData.setStatus(false);
            responseData.setPayload(null);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseData);
        }
        responseData.setStatus(true);
        responseData.setPayload(productService.createNewProduct(product));
        return ResponseEntity.ok(responseData);
    }

}
