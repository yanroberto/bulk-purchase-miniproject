package id.co.miniproject.bulkpurchaseminiproject.controller;


import id.co.miniproject.bulkpurchaseminiproject.model.entity.BulkPurchaseLog;
import id.co.miniproject.bulkpurchaseminiproject.model.entity.Product;
import id.co.miniproject.bulkpurchaseminiproject.model.request.BulkRequst;
import id.co.miniproject.bulkpurchaseminiproject.model.response.APIEventEnum;
import id.co.miniproject.bulkpurchaseminiproject.model.response.ResponseBody;
import id.co.miniproject.bulkpurchaseminiproject.model.response.ResponseInfo;
import id.co.miniproject.bulkpurchaseminiproject.service.BulkTransactionStatusService;
import id.co.miniproject.bulkpurchaseminiproject.usecase.BulkPurchaseUsecase;
import id.co.miniproject.bulkpurchaseminiproject.usecase.StatusPurchaseUsecase;
import id.co.miniproject.bulkpurchaseminiproject.utils.GsonUtil;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@Validated
@Slf4j
public class BulkPurchaseController {

    @Autowired
    private BulkPurchaseUsecase bulkPurchaseUsecase;

    @Autowired
    private StatusPurchaseUsecase statusPurchaseUsecase;

    @Autowired
    private BulkTransactionStatusService bulkTransactionStatusService;



    @Operation(summary = "service to purchase customer phone number bulky")
    @PostMapping("/api/v1/bulk-purchase")
    public ResponseEntity<ResponseBody<Object>> subscriptionCreateOrderBulk(
            @Valid @RequestBody BulkRequst bulkRequst,
            @RequestHeader(name = "ax-request-id") @NotBlank(message = "Header ax-request-id is mandatory") String requestId
            )  {

        log.info("[{}[REQUEST RECEIVED][{}]", APIEventEnum.BULK_PURCHASE, requestId);
        ResponseInfo<Object> responseInfo = bulkPurchaseUsecase.bulkPurchase(requestId,bulkRequst);
        log.info("[{}][{}][COMPLETED REQUEST][{}]", requestId, APIEventEnum.BULK_PURCHASE, GsonUtil.getInstance().toJson(responseInfo));
        return ResponseEntity.status(responseInfo.getHttpStatus())
                .headers(responseInfo.getHttpHeaders())
                .body(responseInfo.getBody());
    }

    @Operation(summary = "This API will return information related to validation status and fulfillment status")
    @GetMapping("/api/v1/bulk-purchase/status/{batch-id}")
    public ResponseEntity<ResponseBody<Object>> getStatusOrderBulk(
            @Parameter(description = "Batch Id ")
            @PathVariable(name = "batch-id", required = true) String batchId){
        log.info("[{}][REQUEST RECEIVED][{}]", APIEventEnum.STATUS_BULK_PURCHASE, batchId);
        ResponseInfo <Object> responseInfo = statusPurchaseUsecase.checkStatusTransaction(batchId);
        log.info("[{}][{}][COMPLETED REQUEST][{}]", batchId, APIEventEnum.STATUS_BULK_PURCHASE, GsonUtil.getInstance().toJson(responseInfo));
        return ResponseEntity.status(responseInfo.getHttpStatus())
                .headers(responseInfo.getHttpHeaders())
                .body(responseInfo.getBody());
    }


}
