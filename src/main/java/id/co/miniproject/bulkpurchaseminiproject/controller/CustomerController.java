package id.co.miniproject.bulkpurchaseminiproject.controller;

import id.co.miniproject.bulkpurchaseminiproject.model.entity.CustomerProfile;
import id.co.miniproject.bulkpurchaseminiproject.model.request.CustomerProfilerRq;
import id.co.miniproject.bulkpurchaseminiproject.model.request.SearchRq;
import id.co.miniproject.bulkpurchaseminiproject.model.response.ResponseData;
import id.co.miniproject.bulkpurchaseminiproject.service.CustomerService;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/customer")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @Autowired
    private ModelMapper modelMapper;


    @Operation(summary = "service to create new customer")
    @PostMapping("create-customer")
    public ResponseEntity<ResponseData<CustomerProfile>> create(@Valid @RequestBody CustomerProfilerRq customerProfilerRq, Errors errors){
        ResponseData<CustomerProfile> responseData = new ResponseData<>();

        if(errors.hasErrors()){
            for(ObjectError error : errors.getAllErrors()) {
                responseData.getMessage().add(error.getDefaultMessage());
            }
            responseData.setStatus(false);
            responseData.setPayload(null);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseData);
        }

        CustomerProfile customerProfile = modelMapper.map(customerProfilerRq,CustomerProfile.class);

        responseData.setStatus(true);
        responseData.setPayload(customerService.createNewCustomer(customerProfile));
        return ResponseEntity.ok(responseData);

    }

    @Operation(summary = "Get All Customer profile Info")
    @GetMapping("/api/productInfo")
    public Iterable<CustomerProfile>findAll(){
        return customerService.findAll();
    }

    @Operation(summary = "Get Customer profile By ID")
    @GetMapping("/{id}")
    public CustomerProfile findOne(@PathVariable("id") Long id){
        return customerService.findOne(id);
    }


    @Operation(summary = "Delete Customer profile By ID")
    @DeleteMapping("/{id}")
    public void removeOne(@PathVariable("id") Long id){
        customerService.removeCustomerProfile(id);
    }

    @Operation(summary = "find Customer profile By name")
    @PostMapping("/search/name")
    public CustomerProfile getProductByName(@RequestBody SearchRq searchRq){
        return customerService.findCustomerByPhoneNumber(searchRq.getSearchKey());

    }

    @Operation(summary = "find Customer profile By name")
    @PostMapping("/search/name-like")
    public List<CustomerProfile> getProductByNameLike(@RequestBody SearchRq searchDTO){
        return customerService.findCustomerByNameLike(searchDTO.getSearchKey());
    }

    @Operation(summary = "Update customer info")
    @PutMapping
    public ResponseEntity<ResponseData<CustomerProfile>> updateProduct(@Valid @RequestBody  CustomerProfile customerProfile, Errors errors){
        ResponseData<CustomerProfile> responseData = new ResponseData<>();

        if(errors.hasErrors()){
            for(ObjectError error : errors.getAllErrors()) {
                responseData.getMessage().add(error.getDefaultMessage());
            }
            responseData.setStatus(false);
            responseData.setPayload(null);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseData);
        }
        responseData.setStatus(true);
        responseData.setPayload(customerService.createNewCustomer(customerProfile));
        return ResponseEntity.ok(responseData);
    }

}
