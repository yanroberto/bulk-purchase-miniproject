package id.co.miniproject.bulkpurchaseminiproject.monitoring;

import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

@Component
public class DatabaseService implements HealthIndicator {

    @Override
    public Health health() {

        if(isDatabaseHealthGood()){
            return Health.
                    up().
                    withDetail("DatabaseService","service is running")
                    .build();
        }else{
            return Health.
                    down().
                    withDetail("DatabaseService","service is not available")
                    .build();
        }

    }

    public boolean isDatabaseHealthGood(){
        //logic check database health
        return true;
    }
}
