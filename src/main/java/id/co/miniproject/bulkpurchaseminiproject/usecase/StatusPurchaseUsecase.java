package id.co.miniproject.bulkpurchaseminiproject.usecase;

import id.co.miniproject.bulkpurchaseminiproject.config.variabel.AppConstant;
import id.co.miniproject.bulkpurchaseminiproject.exception.DataMemberNotFound;
import id.co.miniproject.bulkpurchaseminiproject.model.entity.BulkPurchaseLog;
import id.co.miniproject.bulkpurchaseminiproject.model.response.*;
import id.co.miniproject.bulkpurchaseminiproject.service.BulkTransactionStatusService;
import id.co.miniproject.bulkpurchaseminiproject.service.CustomerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@Slf4j
public class StatusPurchaseUsecase {

    @Autowired
    private  BulkTransactionStatusService bulkTransactionStatusService;


    @Autowired
    private CustomerService createCustomerService;

    public ResponseInfo<Object> checkStatusTransaction(String batchId){

        //========== INIT ==========//
        ResponseInfo<Object> responseInfo = new ResponseInfo<>();
        List<BulkPurchaseLog> list=null;
        List<StatusTransaction>statusTransaction = new ArrayList<>();

        list = bulkTransactionStatusService.findByBatchId(batchId);

        try{
            if(list.isEmpty()){
                throw new DataMemberNotFound();
            }

            //CREATE OBJECT RESPONSE BASED ON SCHEMA
            for(BulkPurchaseLog b :list){
                StatusTransaction gr = new StatusTransaction();

                gr.setPhoneNumber(b.getPhoneNumber());
                gr.setBatchId(b.getBatchId());
                gr.setProductName(b.getProductName());
                gr.setProductCode(b.getProductCode());

                ValidationInfo validationInfo = ValidationInfo.builder().build();
                validationInfo.setStatus(b.getValidationStatus());
                validationInfo.setException(b.getValidationException());
                validationInfo.setDescription(b.getValidationExceptionDescription());
                gr.setValidationInfo(validationInfo);

                FulfillmentInfo fulfillmentInfo= FulfillmentInfo.builder().build();
                fulfillmentInfo.setStatus(b.getOrderStatus());
                fulfillmentInfo.setException(b.getOrderException());
                fulfillmentInfo.setDescription(b.getOrderExceptionDescription());
                gr.setFulfillmentInfo(fulfillmentInfo);

                gr.setSysCreationDate(String.valueOf(b.getCreatedDate()));
                statusTransaction.add(gr);
            }

            log.info("Get Status Bulk Purchase Transaction");
            responseInfo.setSuccess(statusTransaction);
        }catch (Exception e){
            log.error(AppConstant.LOG_ERROR,e);
            responseInfo.setException(e);
        }
//        publishLog(requestInfo, responseInfo);
        return responseInfo;
    }


}

