package id.co.miniproject.bulkpurchaseminiproject.usecase;


import id.co.miniproject.bulkpurchaseminiproject.config.variabel.AppConstant;
import id.co.miniproject.bulkpurchaseminiproject.config.variabel.AppProperties;
import id.co.miniproject.bulkpurchaseminiproject.model.entity.CustomerProfile;
import id.co.miniproject.bulkpurchaseminiproject.model.request.BulkRequst;
import id.co.miniproject.bulkpurchaseminiproject.model.response.ResponseInfo;
import id.co.miniproject.bulkpurchaseminiproject.service.BulkTransactionStatusService;
import id.co.miniproject.bulkpurchaseminiproject.service.CustomerService;
import id.co.miniproject.bulkpurchaseminiproject.service.ProductService;
import id.co.miniproject.bulkpurchaseminiproject.utils.ValidatetionUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.CompletableFuture;

@Component
@Slf4j
public class BulkPurchaseUsecase {

    @Autowired
    private AppProperties appProperties;
    @Autowired
    private ValidatetionUtils validatetionUtils;

    @Autowired
    private BulkTransactionStatusService bulkTransactionStatusService;

    @Autowired
    private ProductService productService;
    @Autowired
    private CustomerService customerService;

    public ResponseInfo<Object> bulkPurchase(String requestId, BulkRequst bulkRequst){

        ResponseInfo<Object> responseInfo = new ResponseInfo<>();
        try{

            /* get migrate process, process in async state */
            List<CompletableFuture<Boolean>> collect = bulkRequst.getPhoneNumberList().
                    stream()
                    .map(it -> CompletableFuture.supplyAsync(() -> doBulkPurchase(bulkRequst,it))).toList();

            /* create a combined Future using allOf() */
            CompletableFuture<Void> future = CompletableFuture.allOf(collect.toArray(new CompletableFuture[collect.size()]));

            /* When all the Futures are completed, call `future.join()` to get their results and collect the results in a list */
            CompletableFuture<List<Boolean>> returnList = future.thenApply(v -> collect.stream().map(CompletableFuture::join).toList());

            /* count the number of success */
            Long successCount = returnList.thenApply(it -> it.stream().filter(r -> r).count()).get();
            Long failedCount = collect.size() - successCount;

            log.info("[BULK PURCHASE ][CORRELATIONID:{}][SUCCESS:{}][FAILED:{}]", requestId, successCount, failedCount);
            responseInfo.setAccepted();

        } catch (InterruptedException ie) {
            log.error(AppConstant.LOG_ERROR,ie);
            Thread.currentThread().interrupt();
        } catch (Exception e) {
            log.error("[catch]Exception:"+e);
            responseInfo.setException(e);

        }
//        publishLog(requestInfo, responseInfo);
        return responseInfo;
    }


    @Cacheable
    public boolean doBulkPurchase(BulkRequst bulkRq, String phoneNumber){

        ResponseInfo<Object> responseInfo = new ResponseInfo<>();
        boolean status= Boolean.FALSE;

        try{
            String productCode = null;
            String productName = null;
            int productPrice = 0;
            
            /*get product info*/
            if(productService.findProductByProductCode(bulkRq.getProductCode()) != null) {
                 productCode = bulkRq.getProductCode();
                 productName = productService.findProductByProductCode(bulkRq.getProductCode()).getProductName();
                 productPrice = (int) productService.findProductByProductCode(bulkRq.getProductCode()).getProductPrice();
            }

                /*validate phone number prefix*/
                boolean checkPrefix = validatetionUtils.checkPrefix(phoneNumber, productName, productCode);

                /*get customer profile*/
                long customerId = customerService.findCustomerByPhoneNumber(phoneNumber).getId();
                String customerName = customerService.findCustomerByPhoneNumber(phoneNumber).getCustomerName();
                String statusNumber = customerService.findCustomerByPhoneNumber(phoneNumber).getStatus();
                String typeCategoryNumber = customerService.findCustomerByPhoneNumber(phoneNumber).getCustomerCategory();
                int balanceCustomer = (int) customerService.findCustomerByPhoneNumber(phoneNumber).getBalance();


                /*validate phone number profile */
                validatetionUtils.validateCustomerProfile(phoneNumber, productName, statusNumber, typeCategoryNumber, productCode);

                /*validate whitelist productId */
                boolean isEligibleCustomer = validatetionUtils.validateProductId(phoneNumber, bulkRq.getProductCode(), productName, productCode);

                /*validate balance customer */
                boolean isBalanceCustomer = false;
                if (balanceCustomer < productPrice) {
                    isBalanceCustomer = validatetionUtils.insufficientBalance(phoneNumber, productName, productCode);
                    isBalanceCustomer = true;
                } else {
                    /*update balance */
                    int balanceUpdate = balanceCustomer - productPrice;
                    CustomerProfile customerProfile = new CustomerProfile();
                    customerProfile.setId(customerId);
                    customerProfile.setCustomerName(customerName);
                    customerProfile.setPhoneNumber(phoneNumber);
                    customerProfile.setStatus(statusNumber);
                    customerProfile.setBalance(balanceUpdate);
                    customerProfile.setCustomerCategory(typeCategoryNumber);
                    customerService.createNewCustomer(customerProfile);
                    validatetionUtils.updateTransaction(phoneNumber, productName, productCode);
                }

                /*check* all validation*/
                status = !checkPrefix && !isBalanceCustomer && !isEligibleCustomer;

        }catch (Exception e){
            log.error("[catch]Exception:"+e);
            responseInfo.setException(e);
        }

        return status;
    }

}


