package id.co.miniproject.bulkpurchaseminiproject.config.swagger;


import id.co.miniproject.bulkpurchaseminiproject.config.variabel.AppConstant;
import io.swagger.v3.oas.models.ExternalDocumentation;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OpenApi {

    @Bean
    public OpenAPI springDoc() {
        return new OpenAPI()
                .info(new Info()
                        .title(AppConstant.APP_NAME)
                        .version(AppConstant.VERSION))
                .externalDocs(new ExternalDocumentation()
                        .description("Mini Project to purchase multiple phone number bulky")
                        .url(""));
    }
}
