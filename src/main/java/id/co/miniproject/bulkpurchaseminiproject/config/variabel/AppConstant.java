package id.co.miniproject.bulkpurchaseminiproject.config.variabel;

public class AppConstant {

    public static final String APP_NAME = "bulk-purchase-miniproject";
    public static final String VERSION = "v1";
    public static final String LOG_ERROR ="Unexpected Error";
    public static final String INFO_INVALID_FORMAT_NUMBER="Invalid Format Number or Prepaid";
    public static final String INFO_INVALID_NUMBERSUSPEND ="Invalid Number is Suspend/Regular Member";
    public static final String INFO_INVALID_PRODUCTID ="Invalid ProductID";


    public static final String SQL_GET_BY_BATCHID =
            "    SELECT id, sys_creation_date, sys_update_date, batch_id, order_exception, order_exception_description, order_status, phone_number, " +
                 "product_code, product_name, validation_exception, validation_exception_description, validation_status\n" +
            "FROM masterdata.bulk_purchase_transaction WHERE BATCH_ID = ?" ;

    public static final String SQL_COUNT_SUCCESS_AND_FAILED_ORDER = "SELECT order_status, COUNT(*) AS total_transactions\n" +
            "    FROM masterdata.bulk_purchase_transaction\n" +
            "    GROUP BY order_status";

    public static final String SQL_ANALYSIS_TABEL_PRODUCT_INFO = "SELECT \n" +
            "    product_code,\n" +
            "    COUNT(*) AS total_products,\n" +
            "    AVG(product_price) AS avg_price,\n" +
            "    MAX(product_price) AS max_price,\n" +
            "    MIN(product_price) AS min_price\n" +
            "FROM \n" +
            "   masterdata.product_info\n" +
            "GROUP BY \n" +
            "    product_code\n" +
            "ORDER BY \n" +
            "    avg_price DESC;"
            ;

    public static final String INFO_INSUFFICIEN_BALANCE = "Pulsa tidak cukup";
    public static final String BEAN_JAKSONMAPPER = "jacksonmapper";

    public enum COMPLETION_STATUS {
        SUCCESS,
        BUSINESS_ERROR,
        SYSTEM_ERROR

    }

    public enum STATUS {
        ok,
        failed,
        error
    }

    public enum VALID {
        OK,
        NOK
    }
}
