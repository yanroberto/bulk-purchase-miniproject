package id.co.miniproject.bulkpurchaseminiproject.config.variabel;

import lombok.Getter;
import lombok.Setter;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@Configuration
public class AppProperties {


    /*list validate phone number prefix*/
    private boolean CHECK_PREFIX_PRODUCT_NUMBER = true;
    private String PREFIX_PRODUCT_NUMBER = "62801;62802;62803;62804;62805";
    private String PREFIX_TRXID_BULK = "BULK-PURCHASE";

    public static final String WHITELIST_PRODUCTID = "BNS10GB;BNS30GB;BNS50GB;BNS55GB";

}

