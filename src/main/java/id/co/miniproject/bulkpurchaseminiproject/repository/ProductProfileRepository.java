package id.co.miniproject.bulkpurchaseminiproject.repository;

import id.co.miniproject.bulkpurchaseminiproject.model.entity.Product;
import jakarta.websocket.server.PathParam;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Repository
public interface ProductProfileRepository extends CrudRepository<Product, Long> {

    @Query("SELECT p FROM Product p WHERE p.productCode= :productCode")
    public Product findProductByProductCode(@PathVariable("productCode") String productCode);

    @Query("SELECT p FROM Product p WHERE p.productName LIKE :productName")
    public List<Product> findProductByNameLike(@PathParam("productName")String productName);

}
