package id.co.miniproject.bulkpurchaseminiproject.repository;

import id.co.miniproject.bulkpurchaseminiproject.config.variabel.AppConstant;
import id.co.miniproject.bulkpurchaseminiproject.model.entity.BulkPurchaseLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface BulkPurchaseTransactionRepository extends JpaRepository<BulkPurchaseLog,Long> {

    @Query(value = AppConstant.SQL_GET_BY_BATCHID, nativeQuery = true)
    List<BulkPurchaseLog> findByBatchId(String batchId);

}