package id.co.miniproject.bulkpurchaseminiproject.repository;


import id.co.miniproject.bulkpurchaseminiproject.model.entity.CustomerProfile;
import jakarta.websocket.server.PathParam;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Repository
public interface CustomerProfileRepository extends CrudRepository<CustomerProfile, Long> {


    @Query("SELECT p FROM CustomerProfile p WHERE p.phoneNumber= :phoneNumber")
    public CustomerProfile findProductByNumber(@PathVariable("phoneNumber") String phoneNumber);


    @Query("SELECT p FROM CustomerProfile p WHERE p.customerName LIKE :customerName")
    public List<CustomerProfile> findProductByNameLike(@PathParam("customerName")String customerName);


}
