package id.co.miniproject.bulkpurchaseminiproject;

import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;

import java.util.TimeZone;

@SpringBootApplication(scanBasePackages = { "id.co.miniproject.bulkpurchaseminiproject"})
@EnableCaching
public class BulkPurchaseMiniprojectApplication {


	public static void main(String[] args) {
		SpringApplication.run(BulkPurchaseMiniprojectApplication.class, args);
	}

	@Bean
	public ModelMapper modelMapper(){
		return new ModelMapper();
	}
	@PostConstruct
	public void init() {
		TimeZone.setDefault(TimeZone.getTimeZone("GMT+7:00"));
	}


}
