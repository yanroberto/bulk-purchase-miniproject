package id.co.miniproject.bulkpurchaseminiproject.utils;


import id.co.miniproject.bulkpurchaseminiproject.config.variabel.AppConstant;
import id.co.miniproject.bulkpurchaseminiproject.config.variabel.AppProperties;
import id.co.miniproject.bulkpurchaseminiproject.exception.CommonException;
import id.co.miniproject.bulkpurchaseminiproject.exception.NotEligbleException;
import id.co.miniproject.bulkpurchaseminiproject.model.entity.BulkPurchaseLog;
import id.co.miniproject.bulkpurchaseminiproject.model.response.ValidationInfo;
import id.co.miniproject.bulkpurchaseminiproject.service.BulkTransactionStatusService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
@Slf4j
public class ValidatetionUtils {

    @Autowired
    private AppProperties appProperties;
    @Autowired
    private BulkTransactionStatusService bulkTransactionStatusService;

    public static String countryCodeValidate(String phoneNumber) {
        if (phoneNumber.startsWith("0", 0)) {
            phoneNumber = "62" + StringUtils.substringAfter(phoneNumber, "0");
        } else if (phoneNumber.startsWith("8", 0)) {
            phoneNumber = "62" + phoneNumber;
        }
        return phoneNumber;
    }

    public boolean checkPrefix(String phoneNumber, String productName,String productCode) throws CommonException {
        String prefix = StringUtils.substring(phoneNumber, 0, 5);
        if (!collectPrefix().contains(prefix)) {
            List<BulkPurchaseLog> bulkPurchaseList = new ArrayList<>();
            String batchId = appProperties.getPREFIX_TRXID_BULK().concat(UUID.randomUUID().toString());
            BulkPurchaseLog bulkPurchaseTransaction = new BulkPurchaseLog();

            bulkPurchaseTransaction.setValidationStatus(AppConstant.VALID.NOK.name());
            bulkPurchaseTransaction.setProductName(productName);
            bulkPurchaseTransaction.setProductCode(productCode);
            bulkPurchaseTransaction.setBatchId(batchId);
            bulkPurchaseTransaction.setPhoneNumber(phoneNumber);
            bulkPurchaseTransaction.setCreatedDate(new Date());
            bulkPurchaseTransaction.setUpdatedDate(new Date());
            bulkPurchaseTransaction.setValidationException("Invalid Prefix");
            bulkPurchaseTransaction.setValidationExceptionDescription(AppConstant.INFO_INVALID_FORMAT_NUMBER);

            /*insert to log */
            bulkPurchaseList.add(bulkPurchaseTransaction);
            bulkTransactionStatusService.saveAll(bulkPurchaseList);
        }
        return false;
    }

    private List<String> collectPrefix() {
        List<String> prefixLists = new ArrayList<>();
        List<String> prefixNumberLists = new ArrayList<>(Arrays.asList(appProperties.getPREFIX_PRODUCT_NUMBER().split(";")));
        if (appProperties.isCHECK_PREFIX_PRODUCT_NUMBER()) {
            prefixLists.addAll(prefixNumberLists);
        }
        return prefixLists;
    }




    public boolean validateProductId(String phoneNumber,String productId,String productName,String productCode) throws NotEligbleException {

        List<String> productIdList = Arrays.asList(AppProperties.WHITELIST_PRODUCTID.split(";"));
        if (!productIdList.contains(productId)) {
            List<BulkPurchaseLog> bulkPurchaseList = new ArrayList<>();
            String batchId = appProperties.getPREFIX_TRXID_BULK().concat(UUID.randomUUID().toString());
            BulkPurchaseLog bulkPurchaseTransaction = new BulkPurchaseLog();

            bulkPurchaseTransaction.setBatchId(batchId);
            bulkPurchaseTransaction.setPhoneNumber(phoneNumber);
            bulkPurchaseTransaction.setProductName(productName);
            bulkPurchaseTransaction.setCreatedDate(new Date());
            bulkPurchaseTransaction.setUpdatedDate(new Date());
            bulkPurchaseTransaction.setProductCode(productCode);

            bulkPurchaseTransaction.setValidationStatus("AppConstant.VALID.NOK.name()");
            bulkPurchaseTransaction.setValidationException(AppConstant.INFO_INVALID_PRODUCTID);
            bulkPurchaseTransaction.setValidationExceptionDescription("Invalid for purchase" + productName + "due invalid ID Product");
            bulkPurchaseTransaction.setOrderStatus(String.valueOf(AppConstant.COMPLETION_STATUS.BUSINESS_ERROR));

            /*insert to database transaction */
            bulkPurchaseList.add(bulkPurchaseTransaction);
            bulkTransactionStatusService.saveAll(bulkPurchaseList);

            throw new NotEligbleException(phoneNumber);
    }
        return false;
    }

    public boolean insufficientBalance(String phoneNumber , String productName, String productCode) throws NotEligbleException {

        List<BulkPurchaseLog> bulkPurchaseList = new ArrayList<>();
        String batchId = appProperties.getPREFIX_TRXID_BULK().concat(UUID.randomUUID().toString());
        BulkPurchaseLog bulkPurchaseTransaction = new BulkPurchaseLog();
        ValidationInfo validationInfo = new ValidationInfo();

        bulkPurchaseTransaction.setBatchId(batchId);
        bulkPurchaseTransaction.setProductCode(productCode);
        bulkPurchaseTransaction.setValidationStatus(AppConstant.VALID.NOK.name());
        bulkPurchaseTransaction.setPhoneNumber(phoneNumber);
        bulkPurchaseTransaction.setProductName(productName);
        bulkPurchaseTransaction.setBatchId(batchId);
        bulkPurchaseTransaction.setCreatedDate(new Date());
        bulkPurchaseTransaction.setUpdatedDate(new Date());
        bulkPurchaseTransaction.setValidationException(AppConstant.INFO_INSUFFICIEN_BALANCE);
        bulkPurchaseTransaction.setValidationExceptionDescription(AppConstant.INFO_INSUFFICIEN_BALANCE);
        bulkPurchaseTransaction.setOrderStatus(String.valueOf(AppConstant.COMPLETION_STATUS.BUSINESS_ERROR));

        /*insert to database transaction */
        bulkPurchaseList.add(bulkPurchaseTransaction);
        bulkTransactionStatusService.saveAll(bulkPurchaseList);

        throw new NotEligbleException(phoneNumber);
    }


    public void validateCustomerProfile(String phoneNumber , String productName ,String statusNumber, String typeCategoryNumber,String productCode) throws NotEligbleException {


        if(!statusNumber.equals("ACTIVE") || typeCategoryNumber.equals("REGULAR")){
            BulkPurchaseLog bulkPurchaseTransaction = new BulkPurchaseLog();
            String batchId = appProperties.getPREFIX_TRXID_BULK().concat(UUID.randomUUID().toString());

            ValidationInfo validationInfo = new ValidationInfo();
            List<BulkPurchaseLog> bulkPurchaseList = new ArrayList<>();

            bulkPurchaseTransaction.setProductCode(productCode);
            bulkPurchaseTransaction.setBatchId(batchId);
            bulkPurchaseTransaction.setValidationStatus(AppConstant.VALID.NOK.name());
            bulkPurchaseTransaction.setPhoneNumber(phoneNumber);
            bulkPurchaseTransaction.setProductName(productName);
            bulkPurchaseTransaction.setBatchId(batchId);
            bulkPurchaseTransaction.setCreatedDate(new Date());
            bulkPurchaseTransaction.setUpdatedDate(new Date());
            validationInfo.setStatus(String.valueOf(AppConstant.INFO_INVALID_NUMBERSUSPEND));
            validationInfo.setException("Desc.customerNotActive");
            validationInfo.setDescription("Subscriber  is not active");
            bulkPurchaseTransaction.setValidationException(AppConstant.INFO_INVALID_NUMBERSUSPEND);
            bulkPurchaseTransaction.setValidationExceptionDescription(AppConstant.INFO_INVALID_NUMBERSUSPEND);
            bulkPurchaseTransaction.setOrderStatus(String.valueOf(AppConstant.COMPLETION_STATUS.BUSINESS_ERROR));


            /*insert log to database */
            bulkPurchaseList.add(bulkPurchaseTransaction);
            bulkTransactionStatusService.saveAll(bulkPurchaseList);
            throw new NotEligbleException(phoneNumber);
        }
    }

    public void updateTransaction(String phoneNumber , String productName,String productCode){
        BulkPurchaseLog bulkPurchaseTransaction = new BulkPurchaseLog();
        String batchId = appProperties.getPREFIX_TRXID_BULK().concat(UUID.randomUUID().toString());

        List<BulkPurchaseLog> bulkPurchaseList = new ArrayList<>();


        bulkPurchaseTransaction.setProductCode(productCode);
        bulkPurchaseTransaction.setBatchId(batchId);
        bulkPurchaseTransaction.setValidationStatus(AppConstant.VALID.OK.name());
        bulkPurchaseTransaction.setPhoneNumber(phoneNumber);
        bulkPurchaseTransaction.setProductName(productName);
        bulkPurchaseTransaction.setCreatedDate(new Date());
        bulkPurchaseTransaction.setUpdatedDate(new Date());
        bulkPurchaseTransaction.setOrderStatus(String.valueOf(AppConstant.COMPLETION_STATUS.SUCCESS));
        /*insert log to database */
        bulkPurchaseList.add(bulkPurchaseTransaction);
        bulkTransactionStatusService.saveAll(bulkPurchaseList);

    }
}
