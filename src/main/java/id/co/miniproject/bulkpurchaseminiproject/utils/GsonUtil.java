package id.co.miniproject.bulkpurchaseminiproject.utils;

import com.google.gson.*;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access= AccessLevel.PRIVATE)
public class GsonUtil {
    private static final Gson gInstance = new GsonBuilder()
            .serializeNulls()
            .setDateFormat("yyyy-MM-dd'T'HH:mm:ss")
            .addSerializationExclusionStrategy(new ExclusionStrategy() {
                @Override
                public boolean shouldSkipClass(Class<?> clazz) {
                    return false;
                }

                @Override
                public boolean shouldSkipField(FieldAttributes field) {
                    return field.getName().contains("stackTrace");
                }
            })
            .create();

    public static synchronized  Gson getInstance() {

        return gInstance;
    }

    public static synchronized  JsonPrimitive getAsJsonPrimitive(JsonObject object, String memberName) {
        final JsonElement element = object.get(memberName);
        return element != null && element.isJsonPrimitive()
                ? element.getAsJsonPrimitive()
                : null;
    }

}