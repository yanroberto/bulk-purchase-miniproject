package id.co.miniproject.bulkpurchaseminiproject.model.request;


import jakarta.validation.constraints.NotEmpty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@ToString
@Accessors(chain = true)
public class CustomerProfilerRq {


    @NotEmpty(message = "Name is required")
    private String customerName;

    private String phoneNumber;

    private String status;

    private int balance;

    private String customerCategory;

}
