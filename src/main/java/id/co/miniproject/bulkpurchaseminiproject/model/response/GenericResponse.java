//package id.co.miniproject.bulkpurchaseminiproject.model.response;
//
//import id.co.miniproject.bulkpurchaseminiproject.exception.CommonException;
//import lombok.Data;
//import lombok.NoArgsConstructor;
//import lombok.experimental.Accessors;
//
//@Data
//@Accessors(chain = true)
//@NoArgsConstructor
//public class GenericResponse<T> {
//    /* ReferenceId & Operation is variable for xcheck reference requestor. It needed when the method called using completable future */
//    private String operation;
//    private String referenceId;
//    private Status status;
//    private T response;
//    private CommonException exception;
//    private long duration = -1;
//
//    public GenericResponse(String operation, Object referenceId) {
//        this.operation = operation;
//        this.referenceId = String.valueOf(referenceId);
//    }
//
//    public GenericResponse(Object referenceId) {
//        this.referenceId = String.valueOf(referenceId);
//    }
//
//    public boolean isOK() {
//        return status.equals(Status.ok);
//    }
//
//    public boolean isNOK() {
//        return !status.equals(Status.ok);
//    }
//
//    /**
//     * set exception using error message
//     * @param errorDescription  error message
//     */
//    public void setException(String errorDescription) {
//        setException(new Exception(errorDescription));
//    }
//
//    public GenericResponse<T> setException(Exception e) {
//        if (e instanceof CommonException) {
//            this.exception = (CommonException) e;
//        } else {
//            this.exception = new CommonException(e);
//        }
//
//        return this;
//    }
//
//    public GenericResponse<T> setStatusOk() {
//        this.status = Status.ok;
//        return this;
//    }
//
//    public GenericResponse<T> setStatusFailed() {
//        this.status = Status.failed;
//        return this;
//    }
//
//    public GenericResponse<T> setStatusError() {
//        this.status = Status.error;
//        return this;
//    }
//
//
//}