package id.co.miniproject.bulkpurchaseminiproject.model.request;


import id.co.miniproject.bulkpurchaseminiproject.utils.ValidatetionUtils;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class PhoneNumber {

    private String phoneNumber;

    public String getNumber(){
        return ValidatetionUtils.countryCodeValidate(phoneNumber);
    }
}
