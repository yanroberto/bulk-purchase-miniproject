package id.co.miniproject.bulkpurchaseminiproject.model.response;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class MediationFaultRs<T> {
    private String rawResponse;
    private T faultObject;
}
