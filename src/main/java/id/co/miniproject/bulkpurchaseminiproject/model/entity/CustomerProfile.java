package id.co.miniproject.bulkpurchaseminiproject.model.entity;


import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serial;
import java.io.Serializable;

@Entity
@Getter
@Setter
@Table(name = "customer_profile", schema = "masterdata")
@NoArgsConstructor
public class CustomerProfile implements Serializable {

    @Serial
    private static final long serialVersionUID =1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;


    @Column(name ="customer_Name")
    private String customerName;


    @Column(name ="phone_number")
    private String phoneNumber;


    @Column(name ="status")
    private String status;


    @Column(name ="customer_balance")
    private double balance;

    @Column(name ="customer_category")
    private String customerCategory;

}
