package id.co.miniproject.bulkpurchaseminiproject.model.entity;


import jakarta.persistence.*;
import jakarta.validation.constraints.NotEmpty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serial;
import java.io.Serializable;

@Getter
@Setter
@Entity
//@Document(indexName = "products")
@Table(name="product_info",schema = "masterdata")
public class Product implements Serializable {

    @Serial
    private static final long serialVersionUID =1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotEmpty(message = "product id is required")
    @Column(name = "product_code" ,length = 100)
    private String productCode;

    @NotEmpty(message = "product name is required")
    @Column(name = "product_name" ,length = 100)
    private String productName;

    @Column(name = "product_description" ,length = 500)
    private String description;

    @Column(name = "product_price")
    private double productPrice;


}
