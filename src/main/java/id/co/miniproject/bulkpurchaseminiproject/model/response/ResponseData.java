package id.co.miniproject.bulkpurchaseminiproject.model.response;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
@Setter
@Getter
public class ResponseData<Request> {

    private boolean status;
    private List<String> message = new ArrayList<>();
    private Request payload;
}
