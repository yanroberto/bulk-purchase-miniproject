package id.co.miniproject.bulkpurchaseminiproject.model.request;

import jakarta.validation.constraints.NotEmpty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;


@Getter
@Setter
@ToString
@Accessors(chain = true)
public class ProductRq {


    @NotEmpty(message = "product code is required")
    private String productCode;

    @NotEmpty(message = "product name is required")
    private String productName;

    private String description;

    private double productPrice;

}
