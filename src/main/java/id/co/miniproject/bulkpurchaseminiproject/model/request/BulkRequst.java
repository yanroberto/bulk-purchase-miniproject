package id.co.miniproject.bulkpurchaseminiproject.model.request;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.List;

@Getter
@Setter
@ToString
@Accessors(chain = true)
public class BulkRequst {

    private String productCode;
    private List<String> phoneNumberList;
}
