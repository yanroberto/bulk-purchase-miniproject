package id.co.miniproject.bulkpurchaseminiproject.model.response;

import id.co.miniproject.bulkpurchaseminiproject.config.variabel.AppConstant;
import lombok.*;

@Getter
@Setter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ApiFault {

    private AppConstant.COMPLETION_STATUS status;
    private String code;
    private String type;
    private String message;
    private String detail;

}
