package id.co.miniproject.bulkpurchaseminiproject.model.request;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class SearchRq {

    private String searchKey;
}
