package id.co.miniproject.bulkpurchaseminiproject.model.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import id.co.miniproject.bulkpurchaseminiproject.config.variabel.AppConstant;
import id.co.miniproject.bulkpurchaseminiproject.exception.CommonException;
import lombok.*;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ResponseInfo<T> {

    private final ResponseBody<T> body = new ResponseBody<>();


    @JsonIgnore
    private HttpStatus httpStatus;
    private HttpHeaders httpHeaders;
    private List<ApiFault> faults;

    @JsonIgnore
    private HttpHeaders headers;

    public boolean isError() {
        return faults != null && !faults.isEmpty() && !httpStatus.is2xxSuccessful();
    }

    public void addHeader(String param, String value) {
        if (this.httpHeaders == null) {
            this.httpHeaders = new HttpHeaders();
        }
        this.httpHeaders.add(param, value);
    }

    public void setSuccess() {
        this.httpStatus = HttpStatus.OK;
        body.setStatus(AppConstant.STATUS.ok);
        body.setCode("00");
        body.setMessage("Success");
    }

    public void setSuccess(String message) {
        this.httpStatus = HttpStatus.OK;
        body.setStatus(AppConstant.STATUS.ok);
        body.setCode("00");
        body.setMessage(message);
    }

    public void setSuccess(T obj) {
        this.httpStatus = HttpStatus.OK;
        body.setStatus(AppConstant.STATUS.ok);
        body.setCode("00");
        body.setMessage("Success");
        body.setData(obj);
    }

    public void setSuccess(String message, T obj) {
        this.httpStatus = HttpStatus.OK;
        body.setStatus(AppConstant.STATUS.ok);
        body.setCode("00");
        body.setMessage(message);
        body.setData(obj);
    }

    public void setException(Exception e) {
        if (e instanceof CommonException) {
            setCommonException((CommonException) e);
        } else {
            setCommonException(new CommonException(e));
        }
    }

    public void setCommonException(CommonException e) {
        this.httpStatus = e.getHttpStatus();
        String status = e.getStatus().equals(AppConstant.COMPLETION_STATUS.SYSTEM_ERROR)?"error":"failed";
        body.setStatus(AppConstant.STATUS.valueOf(status));
        body.setCode(e.getCode());
        body.setMessage(e.getDisplayMessage());
        addException(e);
    }

    public void addException(CommonException e) {
        if (this.faults == null) {
            this.faults = new ArrayList<>();
        }
        this.faults.add(e.getApiFault());
    }

    public void setAccepted() {
        this.httpStatus = HttpStatus.ACCEPTED;
        body.setStatus(AppConstant.STATUS.ok);
        body.setCode("00");
        body.setMessage("request is being process");
    }


}
