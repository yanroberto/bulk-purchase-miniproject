package id.co.miniproject.bulkpurchaseminiproject.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class StatusTransaction {



    @JsonProperty("correlation_id")
    private String batchId;

    @JsonProperty("phone_number")
    private String phoneNumber;

    @JsonProperty ("product_name")
    private String productName;

    @JsonProperty("product_code")
    private String productCode;

    @JsonProperty("validationInfo")
    private ValidationInfo validationInfo;

    @JsonProperty("fulfillmentInfo")
    private FulfillmentInfo fulfillmentInfo;

    @JsonProperty("sys_creation_date")
    private String sysCreationDate;



}

