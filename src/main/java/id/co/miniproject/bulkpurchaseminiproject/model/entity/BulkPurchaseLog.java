package id.co.miniproject.bulkpurchaseminiproject.model.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Immutable;

import java.io.Serial;
import java.io.Serializable;

@Entity
@Getter
@Setter
@Immutable
@Table(name = "bulk_purchase_transaction", schema = "masterdata")
@NoArgsConstructor
public class BulkPurchaseLog extends BaseEntity   implements Serializable {

    @Serial
    private static final long serialVersionUID =1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name ="batch_id")
    private String batchId;

    @Column(name ="phone_number")
    private String phoneNumber;

    @Column(name ="product_code")
    private String productCode;

    @Column(name ="product_name")
    private String productName;

    @Column(name ="validation_status")
    private String validationStatus;

    @Column(name ="validation_exception")
    private String validationException;

    @Column(name ="validation_exception_description")
    private String validationExceptionDescription;

    @Column(name ="order_status")
    private String orderStatus;

    @Column(name ="order_exception")
    private String orderException;

    @Column(name ="order_exception_description")
    private String orderExceptionDescription;


}