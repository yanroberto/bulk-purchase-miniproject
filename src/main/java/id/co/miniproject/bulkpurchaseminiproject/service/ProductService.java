package id.co.miniproject.bulkpurchaseminiproject.service;

import id.co.miniproject.bulkpurchaseminiproject.model.entity.Product;
import id.co.miniproject.bulkpurchaseminiproject.repository.ProductProfileRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ProductService {

    @Autowired
    private ProductProfileRepository productProfileRepository;


    public Product createNewProduct(Product productInfo){
        return productProfileRepository.save(productInfo);
    }

    public Iterable<Product>findAll(){
        return productProfileRepository.findAll();
    }


    public Product findOne(Long id){
        Optional<Product> product = productProfileRepository.findById(id);

        if(!product.isPresent()){
            return null;
        }
        return productProfileRepository.findById(id).get();
    }

    public void removeOne(Long id){
        productProfileRepository.deleteById(id);
    }

    public Product findProductByProductCode(String productCode){
        return productProfileRepository.findProductByProductCode(productCode);
    }

    public List<Product> findProductByNameLike(String productName){
        return productProfileRepository.findProductByNameLike("%"+productName+"%");
    }

}
