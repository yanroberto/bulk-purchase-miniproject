package id.co.miniproject.bulkpurchaseminiproject.service;

import id.co.miniproject.bulkpurchaseminiproject.model.entity.CustomerProfile;
import id.co.miniproject.bulkpurchaseminiproject.repository.CustomerProfileRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class CustomerService {

    @Autowired
    private CustomerProfileRepository customerProfileRepository;


    public CustomerProfile createNewCustomer(CustomerProfile customerProfile){
        return customerProfileRepository.save(customerProfile);
    }

    public Iterable<CustomerProfile>findAll(){
        return customerProfileRepository.findAll();
    }

    public CustomerProfile findOne(Long id){
        Optional<CustomerProfile> customerProfile = customerProfileRepository.findById(id);

        if(!customerProfile.isPresent()){
            return null;
        }
        return customerProfileRepository.findById(id).get();
    }

    public void removeCustomerProfile(Long id){
        customerProfileRepository.deleteById(id);
    }

    public CustomerProfile findCustomerByPhoneNumber(String phoneNumber){
        return customerProfileRepository.findProductByNumber(phoneNumber);
    }


    public List<CustomerProfile> findCustomerByNameLike(String customerName){
        return customerProfileRepository.findProductByNameLike("%"+customerName+"%");
    }
}
