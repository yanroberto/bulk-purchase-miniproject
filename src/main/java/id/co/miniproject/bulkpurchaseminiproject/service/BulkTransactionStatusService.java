package id.co.miniproject.bulkpurchaseminiproject.service;

import id.co.miniproject.bulkpurchaseminiproject.model.entity.BulkPurchaseLog;
import id.co.miniproject.bulkpurchaseminiproject.repository.BulkPurchaseTransactionRepository;
import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class BulkTransactionStatusService {

    @Autowired
    private BulkPurchaseTransactionRepository bulkPurchaseTransactionRepository;


    public void saveAll(List<BulkPurchaseLog> list) {
        bulkPurchaseTransactionRepository.saveAll(list);
    }

    @Transactional
    public List<BulkPurchaseLog> findByBatchId(String batchId) {
        return bulkPurchaseTransactionRepository.findByBatchId(batchId);
    }

}
