##
# [**Bulk-purchase-miniproject**](http://localhost:8080/swagger-ui/index.html#/bulk-purchase-controller/subscriptionCreateOrderBulk)
This API is a mini-project service to create new customers, as well as new product information. Later, 
customers will purchase the product, and there will be customer validation to ensure they can make purchases of the product.

## **APIs Process**
Basically the operations do bulk purchase for eligble customer:
1.  Create new customer insert to database
2.  Create new product insert to database
3.  Customer do purchase bulk
4.  Validate prefix phone number
5.  Get and validate customer profile
6.  Get and validate product info
7.  Insert table transaction 
8.  update validation status ,order status balance
9.  Get Status transcation


## Database KPI
This service also publish the log in `masterdata` at `bulk_purchase_transaction` table