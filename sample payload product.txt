{
  "productCode": "BNS10GB",
  "productName": "BonusPaket10GB",
  "description": "Paket Internetan 30 hari 10 GB",
  "productPrice": 15000
}

{
  "productCode": "BNS30GB",
  "productName": "BonusPaket30GB",
  "description": "Paket Internetan 30 hari 30 GB",
  "productPrice": 25000
}

{
  "productCode": "BNS50GB",
  "productName": "BonusPaket50GB",
  "description": "Paket Internetan 30 hari 50 GB",
  "productPrice": 40000
}

{
  "productCode": "BNS55GB",
  "productName": "BonusPaket55GB",
  "description": "Paket Internetan 30 hari 55 GB",
  "productPrice": 49000
}